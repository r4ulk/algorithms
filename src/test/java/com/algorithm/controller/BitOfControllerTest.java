package com.algorithm.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class BitOfControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private int number = 10; // to convert into binary
    
    private int find = 1; // to find in something
    
    /*
     * Test for return binary representation of an Integer
     * Example: 
     * 		the Binary representation of 10 is 1010
     */
    @Test
    public void shouldTestGetBinaryRepresentation() throws Exception {
    	ResultActions response = mockMvc.perform(get("/bitof/" + number)
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
                .andExpect(content().string("1010"));
    }
    
    /*
     * Test for: given a integer n, convert it into binary representation
     * then count (zeros 0 or ones 1) into binary representation
     * then return count and they positions
     */
    @Test
    public void shouldTestGetBitOfCoubt() throws Exception {
    	ResultActions response = mockMvc.perform(get("/bitof/" + number + "/" + find)
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
                .andExpect(content().string("202"));
    }
    
    /*
     * Test for: given a integer n, convert it into binary representation
     * then max count of consecutive zeros (0) into binary representation
     * then return max of consecutive zeros
     */
    @Test
    public void shouldTestGetGap() throws Exception {
    	ResultActions response = mockMvc.perform(get("/bitof/gaps/" + 1000)
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
                .andExpect(content().string("3"));
    }

}
