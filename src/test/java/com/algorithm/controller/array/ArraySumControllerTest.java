package com.algorithm.controller.array;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ArraySumControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private final String[] numbers = {"1", "5"};
    
    /*
     * Test for SUM of numbers in array
     */
    @Test
    public void shouldSumAllNumbers() throws Exception {
    	ResultActions response = mockMvc.perform(get("/array/sum/" + String.join(",", numbers))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
                .andExpect(content().string("6"));
    }
    
}

