package com.algorithm.controller.array;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ArraySortControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private final String[] arr = {"100", "66", "9", "69", "7", "14", "5", "1"};
    
    /*
     * Test Sort Array using alternative method
     */
    @Test
    public void shouldTestAlternativeSort() throws Exception {
    	ResultActions response = mockMvc.perform(get("/alternativesort/" + String.join(",", arr))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(8)))
				.andExpect(MockMvcResultMatchers.jsonPath("$").value(Matchers.contains(1, 5, 7, 9, 14, 66, 69, 100)));
    }
    
    /*
     * Test Sort Array using Arrays.sort
     */
    @Test
    public void shouldTestSortUsingArrays() throws Exception {
    	ResultActions response = mockMvc.perform(get("/alternativesort/arrays/" + String.join(",", arr))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(8)))
				.andExpect(MockMvcResultMatchers.jsonPath("$").value(Matchers.contains(1, 5, 7, 9, 14, 66, 69, 100)));
    }
}

