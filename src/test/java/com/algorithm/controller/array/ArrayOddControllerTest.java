package com.algorithm.controller.array;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ArrayOddControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private final String[] odd = {"99", "66", "1", "3", "5", "1", "5", "13"};
    
    /*
     * Test for ODD Unpaired number in Array
     */
    @Test
    public void shouldGetOddUnpaired() throws Exception {
    	ResultActions response = mockMvc.perform(get("/odd/unpaired/" + String.join(",", odd))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
    			.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(4)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$").value(Matchers.contains(99, 66, 3, 13)));
    }
    
    /*
     * Test for ODD Unpaired number in Array using Stream, Boxed and Collect
     */
    @Test
    public void shouldGetOddUnpairedUsingStream() throws Exception {
    	ResultActions response = mockMvc.perform(get("/odd/unpaired/stream/" + String.join(",", odd))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(4)))
				.andExpect(MockMvcResultMatchers.jsonPath("$").value(Matchers.contains(99, 66, 3, 13)));
    }
    
}

