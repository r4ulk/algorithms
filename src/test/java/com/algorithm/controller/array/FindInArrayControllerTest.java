package com.algorithm.controller.array;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class FindInArrayControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private final String[] arr = {"99", "66", "1", "3", "5", "7", "8", "13"};
    
    private final String[] firstDuplicated = {"99", "3", "1", "3", "5", "7", "8", "13"};
    
    private final String[] countDuplicated = {"99", "3", "1", "3", "99", "7", "8", "13"};

    /*
     * Test for find in array success result
     */
    @Test
    public void shouldTestFindInArraySuccess() throws Exception {
    	ResultActions response = mockMvc.perform(get("/findinarray/" + String.join(",", arr) + "/99")
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
        		.andExpect(content().string("true"));
    }
    
    /*
     * Test for find in array false result
     */
    @Test
    public void shouldTestFindInArrayFalse() throws Exception {
    	ResultActions response = mockMvc.perform(get("/findinarray/" + String.join(",", arr) + "/666")
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
        		.andExpect(content().string("false"));
    }
    
    /*
     * Test for find in array success result
     * Return positions
     */
    @Test
    public void shouldTestFindInArrayPositionsSuccess() throws Exception {
    	ResultActions response = mockMvc.perform(get("/findinarray/positions/" + String.join(",", firstDuplicated) + "/3")
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(2)))
				.andExpect(MockMvcResultMatchers.jsonPath("$").value(Matchers.contains(1, 3)));
    }
    
    /*
     * Test for find in array false result
     * Return positions
     */
    @Test
    public void shouldTestFindInArrayPositionsFalse() throws Exception {
    	ResultActions response = mockMvc.perform(get("/findinarray/positions/" + String.join(",", arr) + "/666")
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
    			.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(0)));
    }
    /*
     * Test for find in array success result
     */
    @Test
    public void shouldTestFindInArrayStreamSuccess() throws Exception {
    	ResultActions response = mockMvc.perform(get("/findinarray/stream/" + String.join(",", arr) + "/99")
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
        		.andExpect(content().string("true"));
    }
    
    /*
     * Test for find in array false result
     */
    @Test
    public void shouldTestFindInArrayStreamFalse() throws Exception {
    	ResultActions response = mockMvc.perform(get("/findinarray/stream/" + String.join(",", arr) + "/666")
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
        		.andExpect(content().string("false"));
    }
    
    
    /*
     * Test for First Duplicated value in Array
     */
    @Test
    public void shouldTestFirstDuplicated() throws Exception {
    	ResultActions response = mockMvc.perform(get("/findinarray/firstduplicated/" + String.join(",", firstDuplicated))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
				.andExpect(content().string("3"));

    }
    
    /*
     * Test for First Duplicated value in Array using Stream
     */
    @Test
    public void shouldTestFirstDuplicatedStream() throws Exception {
    	ResultActions response = mockMvc.perform(get("/findinarray/firstduplicated/stream/" + String.join(",", firstDuplicated))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
				.andExpect(content().string("3"));

    }
    
    /*
     * Test for Count Duplicated value in Array
     */
    @Test
    public void shouldTestCountDuplicated() throws Exception {
    	ResultActions response = mockMvc.perform(get("/findinarray/countduplicated/" + String.join(",", countDuplicated))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
				.andExpect(content().string("2"));

    }
    
    /*
     * Test for FindAll Duplicated value in Array
     */
    @Test
    public void shouldTestAllDuplicated() throws Exception {
    	ResultActions response = mockMvc.perform(get("/findinarray/allduplicated/" + String.join(",", countDuplicated))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(2)))
				.andExpect(MockMvcResultMatchers.jsonPath("$").value(Matchers.contains(99, 3)));;

    }
    
}

