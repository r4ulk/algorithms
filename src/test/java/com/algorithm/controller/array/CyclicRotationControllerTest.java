package com.algorithm.controller.array;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class CyclicRotationControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private final String[] cyclic = {"1", "2", "3"};
    
    private final int k = 2; // k * times
    
    /*
     * Test a single cyclic rotation of an Array
     */
    /**
     * Given an array return a single cyclic rotation 
     * Example: int[]{ 1,2,3 } needs return int[]{ 3,1,2 } 
     * @param arr - the array
     * @return arr - the array
     * @throws Exception
     */
    @Test
    public void shouldGetCyclicRotation() throws Exception {
    	ResultActions response = mockMvc.perform(get("/cyclic/rotation/" + String.join(",", cyclic))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
    			.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(3)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$").value(Matchers.contains(3, 1, 2)));
    }
    
    /*
     * Test k - times of cyclic rotation of an Array
     */
    /**
     * Given an array return a k*times of a cyclic rotation 
     * Example: int[]{ 1,2,3 } and k = 2, then needs return int[]{ 2,3,1 } 
     * @param arr - the array
     * @return arr - the array
     * @throws Exception
     */
    @Test
    public void shouldGetCyclicRotationKtimes() throws Exception {
    	ResultActions response = mockMvc.perform(get("/cyclic/rotation/" + String.join(",", cyclic) + "/" + k)
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(3)))
				.andExpect(MockMvcResultMatchers.jsonPath("$").value(Matchers.contains(2, 3, 1)));
    }
    
    
}

