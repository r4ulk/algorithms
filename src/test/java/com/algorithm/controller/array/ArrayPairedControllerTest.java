package com.algorithm.controller.array;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ArrayPairedControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private final String[] paired = {"1", "3", "5", "8", "3", "5", "3"};
    
    /*
     * Test for paired number in Array
     */
    /**
     * Paired Position - Given array of numbers returns the paired positions
     * @param arr - The array of Integer comma separated ex. [1, 3, 5, 8, 3, 5, 3]
     * @return int[][] -[[1,4],[1,6],[2,5],[4,1],[4,6],[5,2],[6,1],[6,4]]
     */
    @Test
    public void shouldGetPairedPosition() throws Exception {
    	ResultActions response = mockMvc.perform(get("/paired/position/" + String.join(",", paired))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
    			.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(8)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[0]", IsCollectionWithSize.hasSize(2)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(Matchers.contains(1, 4)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[1]", IsCollectionWithSize.hasSize(2)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[1]").value(Matchers.contains(1, 6)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[2]", IsCollectionWithSize.hasSize(2)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[2]").value(Matchers.contains(2, 5)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[3]", IsCollectionWithSize.hasSize(2)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[3]").value(Matchers.contains(4, 1)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[4]", IsCollectionWithSize.hasSize(2)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[4]").value(Matchers.contains(4, 6)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[5]", IsCollectionWithSize.hasSize(2)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[5]").value(Matchers.contains(5, 2)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[6]", IsCollectionWithSize.hasSize(2)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[6]").value(Matchers.contains(6, 1)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[7]", IsCollectionWithSize.hasSize(2)))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.[7]").value(Matchers.contains(6, 4)));
    }
    
    @Test
    public void shouldGetPairedPositionWithArraySizeOne() throws Exception {
    	ResultActions response = mockMvc.perform(get("/paired/position/1")
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
    			.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(0)));
    }
    
}

