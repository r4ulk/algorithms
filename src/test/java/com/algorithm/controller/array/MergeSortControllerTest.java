package com.algorithm.controller.array;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class MergeSortControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private final String[] arr1 = {"1", "5", "10"};
    
    private final String[] arr2 = {"2", "3", "11"};
    
    /*
     * Test for Selection Sort Array
     */
    @Test
    public void shouldTestSelectionSort() throws Exception {
//    	ResultActions response = mockMvc.perform(get("/mergesort/" + String.join(",", arr1) + "/" + String.join(",", arr2))
//    			.contentType(MediaType.TEXT_HTML));
//    	
//    	response.andExpect(status().isOk())
//    			.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(6)))
//    			.andExpect(MockMvcResultMatchers.jsonPath("$").value(Matchers.contains(1, 2, 3, 5, 10, 11)));
    }
    
}

