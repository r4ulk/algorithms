package com.algorithm.controller.array;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class BulbsControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private final String[] bulbs = {"2", "1", "3", "5", "4"};
    
    /*
     * Test shine moments of bulbs turned on
     */
    @Test
    public void shouldGetBulbsOnShineMoment() throws Exception {
    	ResultActions response = mockMvc.perform(get("/bulbs/onshinemoment/" + String.join(",", bulbs))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
		    	.andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(3)))
				.andExpect(MockMvcResultMatchers.jsonPath("$").value(Matchers.contains(1, 2, 4)));
    }
    
    /*
     * Test shine moments of bulbs turned on
     */
    @Test
    public void shouldGetBulbsOnShineMomentCount() throws Exception {
    	ResultActions response = mockMvc.perform(get("/bulbs/onshinemoment/count/" + String.join(",", bulbs))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
				.andExpect(content().string("3"));
    }
    
}

