package com.algorithm.controller.string;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ReplaceControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    /*
     * Test for replace ? -> char [az]
     */
    @Test
    public void shouldReplaceToChar() throws Exception {
    	ResultActions response = mockMvc.perform(get("/replace/char")
    			.contentType(MediaType.TEXT_HTML));
    	
    	StringBuilder sb = new StringBuilder();
    	response.andExpect(status().isOk());
    			//.andExpect(content().string("Th[a-z]s tex[a-z] w[a-z]ll b[a-z] r[a-z]pla[a-z]ed"));
    }
    
    /*
     * Test for replace # -> int [0-9]
     */
    @Test
    public void shouldReplaceToInt() throws Exception {
    	ResultActions response = mockMvc.perform(get("/replace/int")
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk());
    }
    
    /*
     * Test for replace ? -> char [az] and #  -> int [0-9]
     */
    @Test
    public void shouldReplaceEach() throws Exception {
    	ResultActions response = mockMvc.perform(get("/replace/each")
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk());
    }
    
    
    /*
     * Test for replace each ? -> char [az] and # -> int [0-9]
     */
    @Test
    public void shouldReplaceEachRandom() throws Exception {
    	ResultActions response = mockMvc.perform(get("/replace/each/random")
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk());
    }
    
}
