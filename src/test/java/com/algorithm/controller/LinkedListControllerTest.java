package com.algorithm.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class LinkedListControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private final String[] arr = {"1", "3", "5", "8", "3", "5", "3"};

    /*
     * Test for Sort a LinkedList
     */
    /**
     * sort ASC a LinkedList
     * @param arr - The array of Integer comma separated ex. 1,3,5,8,3,5,3
     * @return sorted LinkedList 1,3,3,3,5,5,8
     */
    @Test
    public void shouldSortLinkedList() throws Exception {
    	ResultActions response = mockMvc.perform(get("/linkedlist/sort/" + String.join(",", arr))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.data").value("1"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.data").value("3"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.data").value("3"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.data").value("3"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.next.data").value("5"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.next.next.data").value("5"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.next.next.next.data").value("8"))
    			;
    }
    
    /*
     * Test for Add Sort a LinkedList
     */
    /**
     * Add sort ASC a LinkedList
     * @param arr - The array of Integer comma separated ex. 1,3,5,8,3,5,3
     * @return sorted LinkedList 1,3,3,3,5,5,8
     */
    @Test
    public void shouldAddSortLinkedList() throws Exception {
    	ResultActions response = mockMvc.perform(get("/linkedlist/addsort/" + String.join(",", arr))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.data").value("1"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.data").value("3"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.data").value("3"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.data").value("3"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.next.data").value("5"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.next.next.data").value("5"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.next.next.next.data").value("8"))
    			;
    }
    
    /*
     * Test for Add Sort DESC a LinkedList
     */
    /**
     * Add sort DESC a LinkedList
     * @param arr - The array of Integer comma separated ex. 1,3,5,8,3,5,3
     * @return sorted LinkedList 1,3,3,3,5,5,8
     */
    @Test
    public void shouldAddSortDescLinkedList() throws Exception {
    	ResultActions response = mockMvc.perform(get("/linkedlist/addsortdesc/" + String.join(",", arr))
    			.contentType(MediaType.TEXT_HTML));
    	
    	response.andExpect(status().isOk())
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.data").value("8"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.data").value("5"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.data").value("5"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.data").value("3"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.next.data").value("3"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.next.next.data").value("3"))
    			.andExpect(MockMvcResultMatchers.jsonPath("$.head.next.next.next.next.next.next.data").value("1"));
    }
}

