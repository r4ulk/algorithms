package com.algorithm.logic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.algorithm.logic.impl.AlgorithmsImpl.User;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AlgorithmsLogicTest {

  private final int MIN_AGE = 10;
  private final int MAX_AGE = 50;
  private final int SMALLER_NUMBER = -11;
  private final int BIGGER_NUMBER = 99;
  private final int[] INT_ARRAY = new int[] { 1, 2, 1, 3, 3, 99, -11 };
  private final String[] STR_ARRAY = new String[] { "a", "b", "a", "c", "d", "d"};

  private final List<User> USER_LIST = Arrays.asList(
                                  new User("C", 30),
                                  new User("D", 40),
                                  new User("A", 10),
                                  new User("B", 20),
                                  new User("E", 50));

  @Autowired
  private AlgorithmsLogic logic;

  @Test
  public void orderReverseByAgeTest() {
    List<User> users = logic.orderReverseByAge(USER_LIST);

    assertThat(users).isNotNull();
    assertThat(users.size()).isEqualTo(5);
    assertThat(users.get(0).getAge()).isEqualTo(MAX_AGE);
  }

  @Test
  public void getBiggerTest() {
    int bigger = logic.getBigger(INT_ARRAY);

    assertThat(bigger).isEqualTo(BIGGER_NUMBER);
  }

  @Test
  public void getSmallerTest() {
    int smaller = logic.getSmaller(INT_ARRAY);

    assertThat(smaller).isEqualTo(SMALLER_NUMBER);
  }

  @Test
  public void countRepeatedStringTest() {
    Map<String, Long> map = logic.countRepeatedString(STR_ARRAY);

    assertThat(map).isNotNull();
    assertThat(map).isNotEmpty();
    assertThat(map.size()).isEqualTo(4);
    assertThat(map.get("a")).isEqualTo(2);
    assertThat(map.get("b")).isEqualTo(1);
    assertThat(map.get("c")).isEqualTo(1);
    assertThat(map.get("d")).isEqualTo(2);
  }

  @Test
  public void unpairedTest() {
    int[] array = new int[] { 9, 3, 9, 3, 9, 7, 9 };
    logic.unpaired(array);
  }

}
