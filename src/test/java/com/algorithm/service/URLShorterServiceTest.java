package com.algorithm.service;

import com.algorithm.exception.MaxLengthException;
import com.algorithm.exception.URLCheckException;
import com.algorithm.service.impl.URLShoterServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
@RunWith(MockitoJUnitRunner.class)
public class URLShorterServiceTest {

  private URLShoterService service = new URLShoterServiceImpl();

  @Test
  public void shouldReturnURLShorterCreated() throws MaxLengthException, URLCheckException {
    String shorterURL = service.shortURL("http://looooong.com/somepath", "MY-NEW-WS");
    assertThat(shorterURL).isEqualTo("http://short.com/MY-NEW-WS");
  }

  @Test
  public void shouldReturnURLShorterCharCreated() throws URLCheckException {
    String shorterURL = service.shortURLRandomCHAR("http://looooong.com/somepath");
    assertTrue(shorterURL.matches("http://short.com/[A-Za-z0-9]{4}"));
  }

  @Test
  public void shouldReturnMaxLengthException() {
    Exception exception = assertThrows(MaxLengthException.class, () -> {
      service.shortURL("http://looooong.com/somepath", "MY-NEW-WS-AAAAA--SSSSSSBBBBBB--SSSSS");
    });
  }

  @Test
  public void shouldReturnURLCheckException() {
    Exception exception = assertThrows(URLCheckException.class, () -> {
      service.shortURL("looooong", "MY-NEW-WS");
    });
  }


}
