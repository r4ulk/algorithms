package com.algorithm.service.impl;

import com.algorithm.exception.MaxLengthException;
import com.algorithm.exception.URLCheckException;
import com.algorithm.service.URLShoterService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
public class URLShoterServiceImpl implements URLShoterService {

  private final String urlBase = "http://short.com/";
  private final Integer MAX_CHAR = 20;

  @Override
  public String shortURL(String url, String shortString) throws MaxLengthException, URLCheckException {
    this.checkStringSize(shortString);
    this.checkURL(url);

    return urlBase + shortString;
  }

  @Override
  public String shortURLRandomCHAR(String url) throws URLCheckException {
    this.checkURL(url);
    return urlBase + RandomStringUtils.randomAlphabetic(4);
  }

  private void checkStringSize(String s) throws MaxLengthException {
    if(s.length() > MAX_CHAR)
      throw new MaxLengthException();
  }

  private void checkURL(String url) throws URLCheckException {
    String regex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    if(!url.matches(regex))
      throw new URLCheckException();

  }

}
