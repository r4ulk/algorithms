package com.algorithm.service;

import com.algorithm.exception.MaxLengthException;
import com.algorithm.exception.URLCheckException;

public interface URLShoterService {
  String shortURL(String url, String shortString) throws MaxLengthException, URLCheckException;
  String shortURLRandomCHAR(String url) throws URLCheckException;

}
