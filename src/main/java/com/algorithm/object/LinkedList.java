package com.algorithm.object;

import com.algorithm.object.Node;

public class LinkedList {
	
	Node head;
  
	public void add(int value) {
		if(head == null) {
			head = new Node(value);
			return;
		}
		Node current = head;
		while(current.next != null) {
			current = current.next;
		}
		current.next = new Node(value);
	}
	
	public void sort() {
      if(head == null) return;
      Node current = head;
      while(current.next != null) {
          if(current.data > current.next.data) {
              int temp = current.data;
              current.data = current.next.data;
              current.next.data = temp;
              current = head;
          }else{
        	  current = current.next;
          }
      }
	}
	
	public void addSort(int value) {
		if(head == null) {
			head = new Node(value);
			return;
		}
		Node current = head;
		while(current.next != null) {
			if(value < current.data) 
				value = swap(value, current);
			
			current = current.next;
		}
		// last node treatment
		// because it.next is null
		if(value < current.data) 
			value = swap(value, current);
		
		current.next = new Node(value);
	}
	
	public void addSortDesc(int value) {
		if(head == null) {
			head = new Node(value);
			return;
		}
		Node current = head;
		while(current.next != null) {
			if(value > current.data) 
				value = swap(value, current);
			
			current = current.next;
		}
		// last node treatment
		// because it.next is null
		if(value > current.data) 
			value = swap(value, current);
		
		current.next = new Node(value);
	}
	
	private int swap(int value, Node current) {
		int temp = current.data;
		current.data = value;
		return temp;
	}

}
