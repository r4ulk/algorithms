package com.algorithm.object;

public class Node {
      
	int data;
	Node next;
  
	Node(int val) {
		this.data = val;
	}
	
}
