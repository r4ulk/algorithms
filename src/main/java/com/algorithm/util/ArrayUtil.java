package com.algorithm.util;

public class ArrayUtil {
	
	/**
	 * copy an array to new array with old length + 1
	 * @param arr  
	 * @return a copy of array with additional position
	 */
	public static int[] copy(int[] arr) {
    	int[] temp = new int[arr.length+1];
    	for(int i = 0; i < arr.length; i++) {
    		temp[i] = arr[i];
    	}
    	return temp;
    }
	
	/*
	 * Swap value of current index to next index
	 */
	public static void swap(int[] arr, int index) {
        int temp = arr[index];
        arr[index] = arr[index + 1];
        arr[index+1] = temp;
    }
	
	/*
	 * Check if an value is present on Array
	 */
	public static Boolean contains(int[] arr, int number) {
		Boolean contain = Boolean.FALSE;
		for(int value : arr) {
			if (value == number) {
				contain = Boolean.TRUE;
				break;
			}
		}
		return contain;
		
	}
	
}
