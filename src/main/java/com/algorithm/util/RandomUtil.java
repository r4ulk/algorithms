package com.algorithm.util;

import java.util.Random;

public class RandomUtil {

	/**
	 * generate random int number between 0-9
	 * @return generate a random int
	 */
	public static int toInt() {
		return new Random().nextInt(10); 
    }
	
	/**
	 * generate random char between a-z
	 * @return generate a random char
	 */
	public static char toChar() {
		return (char) (new Random().nextInt(26) + 'a');
    }
	
}
