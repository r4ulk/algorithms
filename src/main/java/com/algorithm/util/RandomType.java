package com.algorithm.util;

import java.util.Arrays;

public enum RandomType {
	
	INT(0, "#"),
	
	CHAR(1, "?");
	
	private int id;
	
	private String pattern;
	
	private RandomType(int id, String pattern) {
		this.id = id;
		this.pattern = pattern;
	}
	
	public int getId() {
		return id;
	}
	
	public String getPattern() {
		return pattern;
	}
	
	public static RandomType get(String c) {
		return Arrays.asList(RandomType.values()).stream()
					.filter(e -> e.pattern.equals(c))
					.findFirst()
					.orElse(null);
	}
	
}
