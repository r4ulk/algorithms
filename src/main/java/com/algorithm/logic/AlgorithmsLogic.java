package com.algorithm.logic;

import com.algorithm.logic.impl.AlgorithmsImpl.User;
import java.util.List;
import java.util.Map;

public interface AlgorithmsLogic {

	List<User> orderReverseByAge(List<User> users);

	int getBigger(int[] arr);

	int getSmaller(int[] arr);

	Map<String, Long> countRepeatedString(String[] arr);

	int unpaired(int[] arr);

}
