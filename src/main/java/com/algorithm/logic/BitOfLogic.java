package com.algorithm.logic;

import java.util.List;

public interface BitOfLogic {

	List<Integer> bitOf(Integer n);
	
	String bitOfAsString(Integer n);
	
    List<Integer> bitOfCount(Integer n, Integer i);
    
    String bitOfCountAsString(Integer n, Integer i);

    Integer gapCount(Integer n);
}
