package com.algorithm.logic.string;

public interface Random {
	
	String generate();
	
}
