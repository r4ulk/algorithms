package com.algorithm.logic.string;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.algorithm.logic.impl.Char;
import com.algorithm.logic.impl.Int;
import com.algorithm.util.RandomType;

@Component
public class RandomFactory {
	
	private static Map<RandomType, Random> operations;
	
	public RandomFactory() {
		operations = new HashMap<RandomType, Random>();
		operations.put(RandomType.INT, new Int());
		operations.put(RandomType.CHAR, new Char());
	}
	
	public Random from(RandomType randomType) {
		return operations.get(randomType);
	}
	
}
