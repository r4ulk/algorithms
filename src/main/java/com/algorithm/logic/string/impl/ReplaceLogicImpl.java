package com.algorithm.logic.string.impl;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.algorithm.logic.string.RandomFactory;
import com.algorithm.logic.string.ReplaceLogic;
import com.algorithm.util.RandomType;

@Component
public class ReplaceLogicImpl implements ReplaceLogic {
	
	@Autowired
	private RandomFactory randomFactory;
	
	private final String intPattern;
	
	private final String charPattern;
	
	@Value("${string.char}")
	private String textChar;
	
	@Value("${string.int}")
	private String textInt;
	
	@Value("${string.charInt}")
	private String textCharInt;
	
	@Value("${string.charIntRandom}")
	private String textCharIntRandom;
	
	public ReplaceLogicImpl() {
		this.intPattern = String.valueOf(RandomType.INT.getPattern());
		this.charPattern = String.valueOf(RandomType.CHAR.getPattern());
	}

	@Override
	public String replaceToChar() {
		return textChar.replace(charPattern, randomFactory.from(RandomType.CHAR).generate());
	}

	@Override
	public String replaceToInt() {
		return textInt.replace(intPattern, randomFactory.from(RandomType.INT).generate());
	}
	
	@Override
	public String replaceEach() {
		return textCharInt.replace(charPattern, randomFactory.from(RandomType.CHAR).generate())
				   .replace(intPattern, randomFactory.from(RandomType.INT).generate());
	}
	
	@Override
	public String replaceEachRandom() {
		String[] splited = textCharIntRandom.split("");
		StringBuffer sb = new StringBuffer();
		Arrays.asList(splited).stream()
			.forEach(s -> {
				RandomType randomType = RandomType.get(s);
				s = (randomType != null) ? s.replace(randomType.getPattern(), randomFactory.from(randomType).generate()) : s;
				sb.append(s);
			});
		return sb.toString();
	}
}
