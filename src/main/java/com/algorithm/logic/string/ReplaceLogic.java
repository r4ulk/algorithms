package com.algorithm.logic.string;

public interface ReplaceLogic {
	
	String replaceToChar();
	
	String replaceToInt();
	
	String replaceEach();
	
	String replaceEachRandom();

}
