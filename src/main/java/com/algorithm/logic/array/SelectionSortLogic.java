package com.algorithm.logic.array;

public interface SelectionSortLogic {
	
	int[] sort(int[] arr);

}
