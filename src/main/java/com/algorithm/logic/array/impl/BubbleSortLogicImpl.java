package com.algorithm.logic.array.impl;

import org.springframework.stereotype.Component;

import com.algorithm.logic.array.BubbleSortLogic;
import com.algorithm.util.ArrayUtil;

@Component
public class BubbleSortLogicImpl implements BubbleSortLogic {
	
	/**
	 * Bubble Sort
	 * Compare current value with next then swap
	 * @param int[] - array to sort
	 * @return sorted array
	 */
    @Override
    public int[] sort(int[] arr) {
        for(int i = 0; i < arr.length - 1; i++) {
            for(int j = 0; j < arr.length - 1 - i; j++) {
            	if(arr[j] > arr[j+1]) {
            		ArrayUtil.swap(arr, j);
            	}
            }
        }
        return arr;
    }

}
