package com.algorithm.logic.array.impl;

import org.springframework.stereotype.Component;

import com.algorithm.logic.array.SelectionSortLogic;

@Component
public class SelectionSortLogicImpl implements SelectionSortLogic {
	
	/**
	 * Selection Sort
	 * Get the smallest value in array then change with current position
	 * @param int[] - array to sort
	 * @return sorted array
	 */
	@Override
	public int[] sort(int[] arr) {
		for(int i = 0; i < arr.length - 1; i++) {
			int minIndex = i;
			for(int x = i+1; x < arr.length; x++) {
				if(arr[x] < arr[minIndex]) {
					minIndex = x;
				}
			}
			int temp = arr[minIndex];
			arr[minIndex] = arr[i];
			arr[i] = temp;
		}
		return arr;
	}

}
