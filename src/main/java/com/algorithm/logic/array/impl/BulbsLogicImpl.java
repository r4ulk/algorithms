package com.algorithm.logic.array.impl;

import org.springframework.stereotype.Component;

import com.algorithm.logic.array.BulbsLogic;
import com.algorithm.util.ArrayUtil;

@Component
public class BulbsLogicImpl implements BulbsLogic {
    
	@Override
	public int[] bulbsOnShineMoment(int[] arr) {
		int[] moments = new int[] { };
		if(arr.length == 0) return moments;
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == 1 || hasPresent(arr, (arr[i]-1), i)) {
				int[] temp = ArrayUtil.copy(moments);
				temp[moments.length] = i;
				moments = temp;
			}
		}
		return moments;
	}
	
	@Override
	public int bulbsOnShineMomentCount(int[] arr) {
		int moments = 0;
		if(arr.length == 0) return 0;
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == 1) moments++;
			if(hasPresent(arr, (arr[i]-1), i)) {
				moments++;
			}
		}
		return moments;
	}
	
	private Boolean hasPresent(int arr[], int num, int index) {
		for(int i = 0; i < index; i++) {
			if(arr[i] == num) {
				if(num == 1) return Boolean.TRUE;
				return hasPresent(arr, (num-1), index);
			}
		}
		return Boolean.FALSE;
	}
	
}
