package com.algorithm.logic.array.impl;

import org.springframework.stereotype.Component;

import com.algorithm.logic.array.ArrayPairedLogic;

@Component
public class ArrayPairedLogicImpl implements ArrayPairedLogic {
	
	/**
     * Paired Position - Given array of numbers returns the paired positions
     * @param arr - The array of Integer comma separated ex. [1, 3, 5, 8, 3, 5, 3]
     * @return int[][] -[[1,4],[1,6],[2,5],[4,1],[4,6],[5,2],[6,1],[6,4]]
     */
	@Override
	public int[][] pairedPosition(int[] arr) {
		int[][] paired = {};
		if(arr.length == 0 || arr.length == 1) return new int[][] {};
		for(int i = 0; i < arr.length; i++) {
			for(int x = 0; x < arr.length; x++) {
				if(i == arr.length-1 && x == arr.length-1) break;
				if(i == x && x < (arr.length-1)) {
					x++;
				}
				if(arr[i] == arr[x]) {
					int[][] temp = new int[paired.length+1][2];
					temp[paired.length] = new int[] {i, x};
					if(paired.length > 0) {
						for(int k = 0; k < paired.length; k++) {
							temp[k] = paired[k];
						}
					}
					paired = temp;
				}
			}
		}
		return paired;
	}
	
	
}
