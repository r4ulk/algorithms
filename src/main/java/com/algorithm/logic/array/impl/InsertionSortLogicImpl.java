package com.algorithm.logic.array.impl;

import org.springframework.stereotype.Component;

import com.algorithm.logic.array.InsertionSortLogic;

@Component
public class InsertionSortLogicImpl implements InsertionSortLogic {
	
	/**
	 * Insertion Sort
	 * Iterate the array and compare if previous value is greater than current
	 * In a while loop until previous index >= 0
	 * @param int[] - array to sort
	 * @return sorted array
	 */
	@Override
	public int[] sort(int[] arr) {
		for(int i = 0; i < arr.length; i++) {
			int currentValue = arr[i];
			int prevIndex = i - 1;
			while(prevIndex >= 0 && arr[prevIndex] > currentValue) {
				arr[prevIndex+1] = arr[prevIndex];
				prevIndex = prevIndex - 1;
			}
			arr[prevIndex+1] = currentValue;	
		}
		return arr;
	}

}
