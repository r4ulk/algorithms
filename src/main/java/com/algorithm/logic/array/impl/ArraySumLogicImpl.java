package com.algorithm.logic.array.impl;

import org.springframework.stereotype.Component;

import com.algorithm.logic.array.ArraySumLogic;

@Component
public class ArraySumLogicImpl implements ArraySumLogic {
	
	@Override
    public int sum(int[] arr) {
        int sum = 0;
        for(int n : arr) {
            sum += n;
        }
        return sum;
    }
	
}
