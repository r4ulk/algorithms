package com.algorithm.logic.array.impl;

import java.util.Arrays;

import org.springframework.stereotype.Component;

import com.algorithm.logic.array.ArraySortLogic;
import com.algorithm.util.ArrayUtil;

@Component
public class ArraySortLogicImpl implements ArraySortLogic {
    
    @Override
	public int[] alternativeSort(int[] arr) {
    	boolean isSorted = Boolean.FALSE;
    	int lastUnsorted = arr.length - 1;
    	while(!isSorted) {
    		isSorted = Boolean.TRUE;
    		for (int i = 0; i < lastUnsorted; i++) {
    			if(arr[i] > arr[i+1]) {
    				ArrayUtil.swap(arr, i);
    				isSorted = Boolean.FALSE;
    			}
    		}
    		lastUnsorted--;
    	}
    	return arr;
	}

	@Override
	public int[] sortUsingArrays(int[] arr) {
		Arrays.sort(arr);
		return arr;
	}
    
}
