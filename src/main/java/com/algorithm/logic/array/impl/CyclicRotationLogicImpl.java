package com.algorithm.logic.array.impl;

import org.springframework.stereotype.Component;

import com.algorithm.logic.array.CyclicRotationLogic;

@Component
public class CyclicRotationLogicImpl implements CyclicRotationLogic {

	@Override
	public int[] cyclicRotation(int[] arr) {
		int[] newArray = new int[arr.length];
		newArray[0] = arr[arr.length - 1];
		for(int i = 1; i < arr.length; i++) {
			newArray[i] = arr[i-1];
		}
		return newArray;
	}
	
	@Override
	public int[] cyclicRotation(int[] arr, int k) {
		if(arr.length == 0) return arr;
		int[] newArray = new int[arr.length];
		int times = 0;
		while(times < k) {
			newArray[0] = arr[arr.length - 1];
			for(int i = 1; i < arr.length; i++) {
				newArray[i] = arr[i-1];
			}
			arr = newArray;
			newArray = new int[arr.length];
			times++;
		}
		return arr;
	}

}
