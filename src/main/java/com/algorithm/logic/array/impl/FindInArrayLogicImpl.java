package com.algorithm.logic.array.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.algorithm.logic.array.FindInArrayLogic;
import com.algorithm.util.ArrayUtil;

@Component
public class FindInArrayLogicImpl implements FindInArrayLogic {
	
	/**
	 * Find n in Array of int 
	 * @param int[] arr - the array
	 * @param int n - the number to find 
	 * @return Boolean
	 */
	@Override
    public Boolean findInArray(int[] arr, int n) {
        Boolean has = Boolean.FALSE;
		for(int i : arr) {
        	if(i == n) {
        		has = Boolean.TRUE;
        		break;
        	}
        }
		return has;
    }
	
	/**
	 * Find n in Array of int and return they positions
	 * @param int[] arr - the array
	 * @param int n - the number to find 
	 * @return int[] positions - the positions that found the number
	 */
	@Override
    public int[] findInArrayPositions(int[] arr, int n) {
        int[] positions = new int[] {};
		for(int i = 0; i < arr.length; i++) {
        	if(arr[i] == n) {
        		int[] temp = ArrayUtil.copy(positions);
				temp[positions.length] = i;
				positions = temp;
        	}
        }
		return positions;
    }
	
	/**
	 * Find n in Array of int using Stream anyMatch
	 * @param int[] arr - the array
	 * @param int n - the number to find 
	 * @return Boolean
	 */
    @Override
    public Boolean findInArrayStream(Integer[] arr, int n) {
        LinkedList<Integer> sequence = new LinkedList<Integer>(Arrays.asList(arr));
        return sequence.stream().anyMatch(num -> num == n);
    }
    
    /**
	 * Find n in Array of int using Stream anyMatch
	 * @param int[] arr - the array
	 * @param int n - the number to find 
	 * @return Boolean
	 */
    @Override
    public int findFirstDuplicated(int[] arr) {
    	Set<Integer> values = new HashSet<Integer>();
    	int firstDuplicated = -1;
        for(int i = 0; i < arr.length; i++) {
        	if(!values.add(arr[i])) {
        		firstDuplicated = arr[i];
        		break;
        	}
        }
        return firstDuplicated;
    }
    
    @Override
    public Integer findFirstDuplicatedStream(Integer[] arr) {
        Set<Integer> duplicated = new LinkedHashSet<Integer>();
        Optional<Integer> firstDuplicated = Arrays.asList(arr).stream()
                .filter(i -> !duplicated.add(i))
                .findFirst();
        return firstDuplicated.get();
    }

    @Override
    public int countDuplicated(int[] arr) {
        Set<Integer> itens = new HashSet<>();
        int duplicated = 0;
        for (int i = 0; i < arr.length; i++) {
            if (!itens.add(arr[i])) {
            	duplicated++;
            }
        }
        return duplicated;
    }
    
    @Override
    public int[] findAllDuplicated(int[] arr) {
    	int[] duplicated = new int[] { };
    	for(int i = 0; i < arr.length; i++) {
    		for(int j = 0; j < arr.length; j++) {
    			if(i == j) continue;
    			if(arr[i] == arr[j] && !ArrayUtil.contains(duplicated, arr[i])){
    				int[] temp = ArrayUtil.copy(duplicated);
    				temp[duplicated.length] = arr[i];
    				duplicated = temp;
    			}
    		}
    	}
        return duplicated;
    }
    
    @Override
    public int[] findAllDuplicatedStream(int[] arr) {
        return null;
    }
    
}
