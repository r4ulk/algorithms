package com.algorithm.logic.array.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.algorithm.logic.array.ArrayOddLogic;
import com.algorithm.util.ArrayUtil;

@Component
public class ArrayOddLogicImpl implements ArrayOddLogic {
    
    @Override
	public int[] oddUnpaired(int[] arr) {
		int[] unpaireds = new int[] {};
		if(arr.length == 0 || arr.length == 1) return arr;
		for(int i = 0; i < arr.length; i++) {
			Boolean has = Boolean.FALSE;
			for(int x = 0; x < arr.length; x++) {
				if(i == x && x < (arr.length-1)) 
					x++;
				
				if(arr[i] == arr[x] 
						&& i < (arr.length-1)
							&& x < (arr.length-1)) {
					has = Boolean.TRUE;
					break;
				}
			}
			if(!has) {
				int[] temp = ArrayUtil.copy(unpaireds);
				temp[unpaireds.length] = arr[i];
				unpaireds = temp;
			}
		}
		return unpaireds;
	}
    
    @Override
	public int[] oddUnpairedStream(int[] arr) {
		List<Integer> list  = Arrays.stream(arr).boxed().collect( Collectors.toList() );
		return list.stream()
				.filter(i -> Collections.frequency(list, i) == 1)
				.mapToInt(x -> x).toArray();
	}
    
}
