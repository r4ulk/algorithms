package com.algorithm.logic.array;

public interface MergeSortLogic {

    int[] mergeSort(int[] arr1, int[] arr2);
}
