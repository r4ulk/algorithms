package com.algorithm.logic.array;

public interface BubbleSortLogic {

    int[] sort(int[] arr);
    
}
