package com.algorithm.logic.array;

public interface FindInArrayLogic {
	
	Boolean findInArray(int[] arr, int n);
	
	int[] findInArrayPositions(int[] arr, int n);
	
    Boolean findInArrayStream(Integer[] arr, int n);

    int findFirstDuplicated(int[] arr);
    
    Integer findFirstDuplicatedStream(Integer[] arr);

    int countDuplicated(int[] arr);

    int[] findAllDuplicated(int[] arr);
    
    int[] findAllDuplicatedStream(int[] arr);

}
