package com.algorithm.logic.array;

public interface BulbsLogic {

    int[] bulbsOnShineMoment(int[] arr);
    
    int bulbsOnShineMomentCount(int[] arr);
    
}
