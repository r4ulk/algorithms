package com.algorithm.logic.array;

public interface ArraySortLogic {

    int[] alternativeSort(int[] arr);
    
    int[] sortUsingArrays(int[] arr);
    
}
