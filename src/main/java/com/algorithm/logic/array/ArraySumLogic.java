package com.algorithm.logic.array;

public interface ArraySumLogic {

    int sum(int[] arr);

}
