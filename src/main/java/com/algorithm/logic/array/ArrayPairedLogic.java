package com.algorithm.logic.array;

public interface ArrayPairedLogic {

    int[][] pairedPosition(int[] arr);
    
}
