package com.algorithm.logic.array;

public interface CyclicRotationLogic {

    int[] cyclicRotation(int[] arr);
    
    int[] cyclicRotation(int[] arr, int k);

}
