package com.algorithm.logic.array;

public interface InsertionSortLogic {
	
	int[] sort(int[] arr);
	
}
