package com.algorithm.logic.array;

public interface ArrayOddLogic {

    int[] oddUnpaired(int[] arr);
    
    int[] oddUnpairedStream(int[] arr);
    
}
