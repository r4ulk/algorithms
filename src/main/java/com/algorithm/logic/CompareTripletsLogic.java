package com.algorithm.logic;

import java.util.List;

public interface CompareTripletsLogic {

    List<Integer> compare(List<Integer> a, List<Integer> b);
}
