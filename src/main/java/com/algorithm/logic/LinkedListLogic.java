package com.algorithm.logic;

import com.algorithm.object.LinkedList;

public interface LinkedListLogic {
	
	LinkedList fromArray(int[] arr);
	
	LinkedList fromArrayAddSort(int[] arr);
	
	LinkedList fromArrayAddSortDesc(int[] arr);
		
}
