package com.algorithm.logic.impl;

import org.springframework.stereotype.Component;

import com.algorithm.logic.LinkedListLogic;
import com.algorithm.object.LinkedList;

@Component
public class LinkedListLogicImpl implements LinkedListLogic {

	@Override
	public LinkedList fromArray(int[] arr) {
		LinkedList linkedList = new LinkedList();
		for(int i : arr) {
			linkedList.add(i);
		}
		return linkedList;
	}
	
	@Override
	public LinkedList fromArrayAddSort(int[] arr) {
		LinkedList linkedList = new LinkedList();
		for(int i : arr) {
			linkedList.addSort(i);
		}
		return linkedList;
	}
	
	@Override
	public LinkedList fromArrayAddSortDesc(int[] arr) {
		LinkedList linkedList = new LinkedList();
		for(int i : arr) {
			linkedList.addSortDesc(i);
		}
		return linkedList;
	}
	
}
