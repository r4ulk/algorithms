package com.algorithm.logic.impl;

import com.algorithm.logic.AlgorithmsLogic;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.function.Function;

import java.util.Arrays;
@Component
public class AlgorithmsImpl implements AlgorithmsLogic {

    private static Integer[] convertIntoInteger(int[] arr) {
        return Arrays.stream(arr) // IntStream
          .boxed()				// Stream<Integer>
          .toArray(Integer[]::new);

    }

    /*
     * Checks if a number into array is unpaired
     * return the number
     */
    @Override
    public int unpaired(int[] arr) {
        System.out.println( 7 ^ 2);
        int result = 0;
//        for(int i : arr) {
//            result = 2^2;
//            System.out.println(result);
//        }
        return result;

    }

    /*
     * getBigger(new int[] { 1, 2, 1, 3, 3, 99 });
     * returns 99
     */
    public int getBigger(int[] arr){
        Integer[] integers = convertIntoInteger(arr);
        Optional<Integer> result = Stream.of(integers)
          .collect(Collectors.maxBy(Integer::compareTo));

        return result.get();
    }

    /*
     * getSmallest(new int[] { -11, 1, 2, 1, 3, 3, 99 });
     * returns -11
     */
    public int getSmaller(int[] arr){
        Integer[] integers = convertIntoInteger(arr);
        Optional<Integer> result = Stream.of(integers)
          .collect(Collectors.minBy(Integer::compareTo));

        return result.get();
    }

    /*
     * countRepeatedString(new String[] { "a", "b", "a", "c", "d", "d"});
     * returns Map<String, Long> e.g: { key: count }
     */
    public Map<String, Long> countRepeatedString(String[] str){
        Map<String, Long> map = Stream.of(str)
          .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

//        map.forEach((k, v) -> {
//            System.out.println(k + ": " + v);
//        });

        return map;
    }

    /*
     * countRepeatedInteger(new Integer[] { 100, 1, 2, 1, 3, 3, 99 });
     * returns Map<Integer, Long> e.g: { key: count }
     */
    private static void countRepeatedInteger(Integer[] numbers){
        Map<Integer, Long> map = Stream.of(numbers)
          .collect(
            Collectors.groupingBy(Function.identity(), Collectors.counting()
            ))
          // Way to Ordering the Map
          .entrySet()
          .stream()
          // By Key
          //.sorted(Map.Entry.comparingByKey())
          // By Value
          .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
          .collect(
            Collectors.toMap(
              Map.Entry::getKey,
              Map.Entry::getValue,
              (oldValue, newValue) -> oldValue, LinkedHashMap::new
            )
          );

        map.forEach((k, v) -> {
            System.out.println(k + ": " + v);
        });
    }

    /*
     * sumDigitsReduce(198);
     * returns 18
     */
    private static void sumDigitsReduce(int number){
        String[] s = String.valueOf(number).split("");
        int sum = Stream.of(s)
          .map(n -> Integer.valueOf(n))
          .reduce(0, (a, b) -> a + b);
        System.out.println(sum);
    }

    private static void mapIntToString(){
        List<Integer> integers = buildListIntegers();
        String result = integers
          .stream()
          .map(n -> n.toString())
          .reduce("", String::concat);
        System.out.println(result);
    }

    private static void sumReduce(){
        List<Integer> integers = buildListIntegers();
        Integer result = integers
          .stream()
          .reduce(0, Integer::sum);
        //.reduce(0, (a, b) -> a + b);
        System.out.println(result);
    }

    private static void equality() {
        String s = new String("equality"); //    ==  false
        String t = new String("equality"); // equals true

        //String s = "equality"; //    ==  true
        //String t = "equality"; // equals true

        //String s = new String("equality"); //    ==  true
        //String t = s;                      // equals true

        System.out.println(s == t);
        System.out.println(s.equals(t));

    }

    private static List<Integer> buildListIntegers() {
        List<Integer> list = new ArrayList<Integer>();
        int i = 0;
        while( i <= 10 ) {
            list.add(i);
            i++;
        }
        return list;
    }

    private static void streamOfInteger() {
        //Stream<Integer>
        Stream
          .of(1, 2, 3, 4, 5)
          .forEach(System.out::println);
    }

    private static void streamOfArray() {
        //Stream<Integer>
        Stream
          .of(new Integer[] { 10, 2, 3, 4, 5 })
          .forEach(System.out::println);
    }

    private static void streamGenerateDate() {
        //Stream<Date>
        Stream
          .generate(Date::new)
          .limit(5)
          .forEach(System.out::println);
    }

    private static void intStream() {
        //IntStream
        "12345_abcdefg"
          .chars()
          .forEach(System.out::println);
    }

    private static void streamString() {
        //Stream<String>
        Stream
          .of("A$B$C".split("\\$"))
          .forEach(System.out::println);
    }

    private static void streamToList() {
        //List<Integer>
        List<Integer> integers = buildListIntegers();
        System.out.println(integers.size());
        List<Integer> list = integers
          .stream()
          .filter(i -> i%2 == 0)
          .collect(Collectors.toList());
        System.out.println(list);
    }

    private static void streamToArray() {
        //List<Integer>
        List<Integer> integers = buildListIntegers();
        Integer[] arr = integers
          .stream()
          .filter(i -> i%2 == 0)
          .toArray(Integer[]::new);
        System.out.println(arr.length);
    }

    private static void intStreamRange() {
        //IntStream of int primitive Range 1, 5 Excludent
        IntStream
          .range(1, 5)
          .forEach(System.out::println);
    }

    private static void intStreamRangeClosed() {
        //IntStream of int primitive Range 1, 5 Includent
        IntStream
          .rangeClosed(1, 5)
          .forEach(System.out::println);
    }

    private static void intStreamIterator() {
        // IntStream
        IntStream
          .iterate(0, i -> i+1)
          .limit(5)
          .forEach(System.out::println);
    }

    private static void intStreamMap() {
        // IntStream
        IntStream
          .range(1, 5)
          .map(i -> i * i)
          .forEach(System.out::println);
    }

    public List<User> orderReverseByAge(List<User> users) {
        List<User> ordered = users.stream()
          .sorted(Comparator.comparingInt(User::getAge).reversed())
          .collect(Collectors.toList());

        return ordered;
    }

    public static class User {

        private String name;
        private int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
              "name='" + name + '\'' +
              ", age=" + age +
              '}';
        }
    }

}
