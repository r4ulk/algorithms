package com.algorithm.logic.impl;

import org.springframework.stereotype.Component;

import com.algorithm.logic.string.Random;

@Component
public class Int implements Random {

	public Int() {
		
	}
	
	@Override
	public String generate() {
		int number = new java.util.Random().nextInt(10);
		return String.valueOf(number);
	}

}
