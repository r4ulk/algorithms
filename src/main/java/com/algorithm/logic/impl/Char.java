package com.algorithm.logic.impl;

import org.springframework.stereotype.Component;

import com.algorithm.logic.string.Random;

@Component
public class Char implements Random {

	public Char() {
		
	}

	@Override
	public String generate() {
		char c = (char) (new java.util.Random().nextInt(26) + 'a');
		return String.valueOf(c);
	}

}
