package com.algorithm.logic.impl;

import com.algorithm.logic.CompareTripletsLogic;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

@Component
public class CompareTripletsLogicImpl implements CompareTripletsLogic {

    @Override
    public List<Integer> compare(List<Integer> a, List<Integer> b) {
        List<Integer> score = new ArrayList<Integer>(Arrays.asList(0,0));
        if(a.size() == b.size()) {
           IntStream.range(0, a.size())
                   .forEach(index -> {
                       if(a.get(index) > b.get(index)) {
                           score.set(0, score.get(0)+1);
                       }else if(b.get(index) > a.get(index)){
                           score.set(1, score.get(1)+1);
                       }
                   });
        }
        return score;
    }
}
