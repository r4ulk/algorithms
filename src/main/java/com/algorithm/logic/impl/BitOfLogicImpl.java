package com.algorithm.logic.impl;

import com.algorithm.logic.BitOfLogic;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class BitOfLogicImpl implements BitOfLogic {

    @Override
    public String bitOfAsString(Integer n) {
        List<Integer> bit = new ArrayList<>();
        while ((n/2) > 0) {
            bit.add(n % 2);
            n = n/2;
        }
        bit.add(n);
        Collections.reverse(bit);
        return bit.stream()
      	      .map(x -> String.valueOf(x))
      	      .collect(Collectors.joining());
    }
    
    /**
     * Binary Representation of a number
     * @param n - The number integer
     * @return List of Integer - Binary representation of a number n
     */
    @Override
    public List<Integer> bitOf(Integer n) {
        List<Integer> bit = new ArrayList<>();
        while ((n/2) > 0) {
            bit.add(n % 2);
            n = n/2;
        }
        bit.add(n);
        Collections.reverse(bit);
        return bit;
    }

    /**
     * Count of a number (0 or 1) into binary representation
     * @param n - The number to get a binary representation
     * @param i - The number to find in binary representation
     * @return List of Integer composed:
     *         - First element of List represents the Count that find i in binary representation
     *         - Rest elements represents they positions that find i in binary representation
     */
    @Override
    public String bitOfCountAsString(Integer n, Integer i) {
        List<Integer> bit = this.bitOf(n);
        List<Integer> response = new ArrayList<Integer>();
        Long count = bit.stream().filter(num -> num == i).count();
        response.add(count.intValue());
        int[] indexes = IntStream.range(0, bit.size())
                .filter(num -> bit.get(num) == i)
                .toArray();
        response.addAll(Arrays.stream(indexes).boxed().collect(Collectors.toList()));
        return response.stream()
        	      .map(x -> String.valueOf(x))
          	      .collect(Collectors.joining());
    }
    
    /**
     * Count of a number (0 or 1) into binary representation
     * @param n - The number to get a binary representation
     * @param i - The number to find in binary representation
     * @return List of Integer composed:
     *         - First element of List represents the Count that find i in binary representation
     *         - Rest elements represents they positions that find i in binary representation
     */
    @Override
    public List<Integer> bitOfCount(Integer n, Integer i) {
        List<Integer> bit = this.bitOf(n);
        List<Integer> response = new ArrayList<Integer>();
        Long count = bit.stream().filter(num -> num == i).count();
        response.add(count.intValue());
        int[] indexes = IntStream.range(0, bit.size())
                .filter(num -> bit.get(num) == i)
                .toArray();
        response.addAll(Arrays.stream(indexes).boxed().collect(Collectors.toList()));
        return response;
    }

    /**
     * Maximal sequence of GAP (Consecutive Zeros) into a binary representation
     * @param n - The number to get a binary representation
     * @return Integer count of maximal consecutive zeros
     */
    @Override
    public Integer gapCount(Integer n) {
        List<Integer> bit = this.bitOf(n);
        Integer maxGap = 0;
        List<Integer> gap = new ArrayList<>();
        for (Integer x : bit) {
            if (x == 0) {
                gap.add(x);
            }else{
            	maxGap = (gap.size() > maxGap) ? gap.size() : maxGap;
            	if(gap.size() > 0)
            		gap = new ArrayList<>();
            }
        }
        return (gap.size() > maxGap) ? gap.size() : maxGap;
    }
}
