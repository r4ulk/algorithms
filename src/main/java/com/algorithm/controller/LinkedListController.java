package com.algorithm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.algorithm.logic.LinkedListLogic;
import com.algorithm.object.LinkedList;
import com.google.gson.Gson;


@RestController
@RequestMapping(value = "/linkedlist")
public class LinkedListController {

    @Autowired
    private LinkedListLogic linkedListLogic;
    
    private Gson gson = new Gson();

    /**
     * sort ASC a LinkedList
     * @param arr - The array of Integer comma separated ex. 1,3,5,8,3,5,3
     * @return sorted LinkedList 1,3,3,3,5,5,8
     */
    @RequestMapping(value = "/sort/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> sort(@PathVariable int[] arr) {
    	LinkedList linkedList = linkedListLogic.fromArray(arr);
    	linkedList.sort();
        return ResponseEntity.ok(gson.toJson(linkedList));
    }
    
    /**
     * Add Sort ASC a LinkedList
     * @param arr - The array of Integer comma separated ex. 1,3,5,8,3,5,3
     * @return sorted LinkedList 1,3,3,3,5,5,8
     */
    @RequestMapping(value = "/addsort/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> addSort(@PathVariable int[] arr) {
    	LinkedList linkedList = linkedListLogic.fromArrayAddSort(arr);
        return ResponseEntity.ok(gson.toJson(linkedList));
    }
    
    /**
     * Add Sort DESC a LinkedList
     * @param arr - The array of Integer comma separated ex. 1,3,5,8,3,5,3
     * @return sorted LinkedList 8,5,5,3,3,3,1
     */
    @RequestMapping(value = "/addsortdesc/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> addsortDesc(@PathVariable int[] arr) {
    	LinkedList linkedList = linkedListLogic.fromArrayAddSortDesc(arr);
    	return ResponseEntity.ok(gson.toJson(linkedList));
    }

}
