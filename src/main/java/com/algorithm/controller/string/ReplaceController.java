package com.algorithm.controller.string;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.algorithm.logic.string.ReplaceLogic;
import com.google.gson.Gson;

@RestController
@RequestMapping(value = "/replace")
public class ReplaceController {

    @Autowired
    private ReplaceLogic logic;
    
    private Gson gson = new Gson();

    @RequestMapping(value = "/char", method = RequestMethod.GET)
    public ResponseEntity<String> replaceToChar() {
    	return ResponseEntity.ok(gson.toJson(logic.replaceToChar()));
    }
    
    @RequestMapping(value = "/int", method = RequestMethod.GET)
    public ResponseEntity<String> replaceToInt() {
    	return ResponseEntity.ok(gson.toJson(logic.replaceToInt()));
    }
    
    @RequestMapping(value = "/each", method = RequestMethod.GET)
    public ResponseEntity<String> replaceEach() {
    	return ResponseEntity.ok(gson.toJson(logic.replaceEach()));
    }
    
    @RequestMapping(value = "/each/random", method = RequestMethod.GET)
    public ResponseEntity<String> replaceEachRadom() {
    	return ResponseEntity.ok(gson.toJson(logic.replaceEachRandom()));
    }

}
