package com.algorithm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import com.algorithm.logic.BitOfLogic;

@RestController
@RequestMapping(value = "/bitof")
public class BitOfController {

    @Autowired
    private BitOfLogic bo;
    
    /**
     * @return Binary representation of an integer
     */
    @RequestMapping(value = "/{n}", method = RequestMethod.GET)
    public ResponseEntity<String> bitOf(@PathVariable Integer n) {
        return ResponseEntity.ok(bo.bitOfAsString(n));
    }
    
    /**
     * @return total of n (0 or 1) into a Binary representation of an integer
     * and they positions
     * 
     */
    @RequestMapping(value = "/{n}/{i}", method = RequestMethod.GET)
    public ResponseEntity<String> bitOfCount(@PathVariable Integer n, @PathVariable Integer i) {
        return ResponseEntity.ok(bo.bitOfCountAsString(n, i));
    }
    
    /**
     * Maximal sequence of GAP (Consecutive Zeros) into a binary representation
     * @param n - The number to get a binary representation
     * @return Integer count of maximal consecutive zeros
     */
    @RequestMapping(value = "/gaps/{n}", method = RequestMethod.GET)
    public ResponseEntity<Integer> gapCount(@PathVariable Integer n) {
        return ResponseEntity.ok(bo.gapCount(n));
    }
}
