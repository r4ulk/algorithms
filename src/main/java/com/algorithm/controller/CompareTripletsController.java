package com.algorithm.controller;

import com.algorithm.logic.CompareTripletsLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/triplets")
public class CompareTripletsController {

    @Autowired
    CompareTripletsLogic bo;

    /**
     * Based on challenge of Hacker Rank - https://www.hackerrank.com/challenges/compare-the-triplets/problem
     * @param a points comma separated
     * @param b   points comma separated
     * @return List of integer with points alice and bob points ex.: [1, 1]
     */
    @RequestMapping(value = "/{a}/{b}", method = RequestMethod.GET)
    public List<Integer> compareTriplets(@PathVariable List<Integer> a, @PathVariable List<Integer> b) {
        return bo.compare(a, b);
    }

}
