package com.algorithm.controller.array;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.algorithm.logic.array.MergeSortLogic;
import com.google.gson.Gson;

@RestController
@RequestMapping(value = "/mergesort")
public class MergeSortController {

    @Autowired
    private MergeSortLogic logic;
    
    private Gson gson = new Gson();

    @RequestMapping(value = "/collection/{arr1}/{arr2}", method = RequestMethod.GET)
    public ResponseEntity<String> mergeSortCollection(@PathVariable int[] arr1, @PathVariable int[] arr2) {
        return ResponseEntity.ok(gson.toJson(logic.mergeSort(arr1, arr2)));
    }

}
