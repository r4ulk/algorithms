package com.algorithm.controller.array;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.algorithm.logic.array.ArrayOddLogic;
import com.google.gson.Gson;


@RestController
@RequestMapping(value = "/odd/unpaired")
public class ArrayOddController {

    @Autowired
    private ArrayOddLogic logic;
    
    private Gson gson = new Gson();
    
    /**
     * Odd - given array of odd numbers returns unpaired number value
     * Example: int[]{ 1,2,3,1,2 }  should return {3}
     * @param  int[] arr - The array of int
     * @return int[] arr - unpaired numbers
     */
    @RequestMapping(value = "/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> oddUnpaired(@PathVariable int[] arr) {
        return ResponseEntity.ok(gson.toJson(logic.oddUnpaired(arr)));
    }
    
    /**
     * Odd - given array of odd numbers returns unpaired number value
     * Example: int[]{ 1,2,3,1,2 } should return {3}
     * @param  int[] arr - The array of int
     * @return int[] arr - unpaired numbers
     */
    @RequestMapping(value = "/stream/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> oddUnpairedStream(@PathVariable int[] arr) {
        return ResponseEntity.ok(gson.toJson(logic.oddUnpairedStream(arr)));
    }

}
