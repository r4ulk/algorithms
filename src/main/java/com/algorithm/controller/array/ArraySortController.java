package com.algorithm.controller.array;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.algorithm.logic.array.ArraySortLogic;
import com.google.gson.Gson;


@RestController
@RequestMapping(value = "/alternativesort")
public class ArraySortController {

    @Autowired
    private ArraySortLogic logic;
    
    private Gson gson = new Gson();
    
    /**
     * given array of numbers returns sorted array
     */
    @RequestMapping(value = "/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> alternativeSort(@PathVariable int[] arr) {
        return ResponseEntity.ok(gson.toJson(logic.alternativeSort(arr)));
    }
    
    /**
     * given array of numbers returns sorted array using Arrays
     */
    @RequestMapping(value = "/arrays/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> sortUsingArrays(@PathVariable int[] arr) {
        return ResponseEntity.ok(gson.toJson(logic.sortUsingArrays(arr)));
    }

}
