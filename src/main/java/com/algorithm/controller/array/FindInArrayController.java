package com.algorithm.controller.array;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.algorithm.logic.array.FindInArrayLogic;
import com.google.gson.Gson;

@RestController
@RequestMapping(value = "/findinarray")
public class FindInArrayController {

    @Autowired
    private FindInArrayLogic logic;

    private Gson gson = new Gson();

    /**
     * Find n in array of Integer
     * @param arr - The array of Integer comma separated ex. 300,66,89,7,15,1
     * @param n - The number integer to find ex. 300
     * @return Boolean
     */
    @RequestMapping(value = "/{arr}/{n}", method = RequestMethod.GET)
    public ResponseEntity<String> findInArray(@PathVariable int[] arr, @PathVariable int n) {
    	return ResponseEntity.ok(gson.toJson(logic.findInArray(arr, n)));
    }
    
    /**
     * Find n in array of Integer
     * @param arr - The array of Integer comma separated ex. 300,66,89,7,15,1
     * @param n - The number integer to find ex. 300
     * @return Postions - int[]
     */
    @RequestMapping(value = "/positions/{arr}/{n}", method = RequestMethod.GET)
    public ResponseEntity<String> findInArrayPositions(@PathVariable int[] arr, @PathVariable int n) {
    	return ResponseEntity.ok(gson.toJson(logic.findInArrayPositions(arr, n)));
    }
    
    /**
     * Find n in array of int using Stream
     * @param arr - The array of Integer comma separated ex. 300,66,89,7,15,1
     * @param n - The number integer to find ex. 300
     * @return Boolean
     */
    @RequestMapping(value = "/stream/{arr}/{n}", method = RequestMethod.GET)
    public ResponseEntity<String> findInArrayStream(@PathVariable Integer[] arr, @PathVariable int n) {
    	return ResponseEntity.ok(gson.toJson(logic.findInArrayStream(arr, n)));
    }
    

    /**
     * Find first duplicated integer in Array
     * @param arr - The array of Integer comma separated ex. 100,1,100,99,120,33
     * @return Integer
     */
    @RequestMapping(value = "/firstduplicated/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> findFirstDuplicated(@PathVariable int[] arr) {
    	return ResponseEntity.ok(gson.toJson(logic.findFirstDuplicated(arr)));
    }

    /**
     * Find first duplicated integer in Array
     * @param arr - The array of Integer comma separated ex. 100,1,100,99,120,33
     * @return Integer
     */
    @RequestMapping(value = "/firstduplicated/stream/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> findFirstDuplicatedStream(@PathVariable Integer[] arr) {
    	return ResponseEntity.ok(gson.toJson(logic.findFirstDuplicatedStream(arr)));
    }
    
    /**
     * Count duplicated integer in Array
     * @param arr - The array of Integer comma separated ex. 100,1,100,99,120,33
     * @return Integer
     */
    @RequestMapping(value = "/countduplicated/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> countDuplicated(@PathVariable int[] arr) {
    	return ResponseEntity.ok(gson.toJson(logic.countDuplicated(arr)));
    }


    /**
     * Find all duplicated integers in Array
     * @param arr - The array of Integer comma separated ex. 100,1,100,99,120,33
     * @return List<Integer> - List integer duplicated
     */
    @RequestMapping(value = "/allduplicated/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> findAllDuplicated(@PathVariable int[] arr) {
    	return ResponseEntity.ok(gson.toJson(logic.findAllDuplicated(arr)));
    }

}
