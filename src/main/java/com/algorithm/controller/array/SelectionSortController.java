package com.algorithm.controller.array;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.algorithm.logic.array.SelectionSortLogic;
import com.google.gson.Gson;

@RestController
@RequestMapping(value = "/selectionsort")
public class SelectionSortController {

    @Autowired
    private SelectionSortLogic logic;
    
    private Gson gson = new Gson();

    @RequestMapping(value = "/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> sort(@PathVariable int[] arr) {
    	return ResponseEntity.ok(gson.toJson(logic.sort(arr)));
    }

}
