package com.algorithm.controller.array;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.algorithm.logic.array.BulbsLogic;
import com.google.gson.Gson;


@RestController
@RequestMapping(value = "/bulbs")
public class BulbsController {

    @Autowired
    private BulbsLogic logic;
    
    private Gson gson = new Gson();

    /**
     * Bulbs on shine moments - Given array of numbers returns the moments that bulbs shine
     * return the moments that bulbs shine in sequence number
     * @param arr - The array of Integer comma separated ex. 1, 2, 4, 3
     * @return int[] { 0, 1, 3 }
     */
    @RequestMapping(value = "/onshinemoment/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> bulbsOnShineMoment(@PathVariable int[] arr) {
        return ResponseEntity.ok(gson.toJson(logic.bulbsOnShineMoment(arr)));
    }
    
    /**
     * Bulbs on shine moments - Given array of numbers returns the total moments that bulbs shine
     * return the total of moments that bulbs shine in sequence number
     * @param arr - The array of Integer comma separated ex. 1, 2, 4, 3
     * @return int - the total moments
     */
    @RequestMapping(value = "/onshinemoment/count/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> bulbsOnShineMomentCount(@PathVariable int[] arr) {
        return ResponseEntity.ok(gson.toJson(logic.bulbsOnShineMomentCount(arr)));
    }
    
}
