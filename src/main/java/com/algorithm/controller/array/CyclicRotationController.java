package com.algorithm.controller.array;

import com.algorithm.logic.array.CyclicRotationLogic;
import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/cyclic/rotation")
public class CyclicRotationController {

    @Autowired
    private CyclicRotationLogic logic;
    
    private Gson gson = new Gson();

    
    /**
     * Cyclic rotation - Given array of numbers, then returns cyclic rotation of array
     * Example: arr[]{1, 2, 3} then should return int[]{3, 1, 2}
     * @param arr - The array of Integer comma separated ex. [1, 2, 3]
     * @return int[] array
     */
    @RequestMapping(value = "/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> cyclicRotation(@PathVariable int[] arr) {
        return ResponseEntity.ok(gson.toJson(logic.cyclicRotation(arr)));
    }
    
    /**
     * Cyclic rotation  - Given array of numbers, then returns cyclic rotation of array
     * Example: arr[]{1, 2, 3} and k = 1, then should return int[]{3, 1, 2}
     * @param arr - The array of Integer comma separated ex. [1, 2, 3]
     * @param   k - times
     * @return int[] array
     */
    @RequestMapping(value = "/{arr}/{k}", method = RequestMethod.GET)
    public ResponseEntity<String> cyclicRotation(@PathVariable int[] arr, @PathVariable int k) {
        return ResponseEntity.ok(gson.toJson(logic.cyclicRotation(arr, k)));
    }


}
