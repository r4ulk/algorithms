package com.algorithm.controller.array;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.algorithm.logic.array.ArraySumLogic;
import com.google.gson.Gson;


@RestController
@RequestMapping(value = "/array")
public class ArraySumController {

    @Autowired
    private ArraySumLogic logic;
    
    private Gson gson = new Gson();

    /**
     * Sum all numbers of array
     * @param  arr - The array of Integer comma separated ex. 300,66,89,7,15,1
     * @return int - sum
     */
    @RequestMapping(value = "/sum/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> sum(@PathVariable int[] arr) {
    	return ResponseEntity.ok(gson.toJson(logic.sum(arr)));
    }

}
