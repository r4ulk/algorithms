package com.algorithm.controller.array;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.algorithm.logic.array.ArrayPairedLogic;
import com.google.gson.Gson;


@RestController
@RequestMapping(value = "/paired/")
public class ArrayPairedController {

    @Autowired
    private ArrayPairedLogic logic;
    
    private Gson gson = new Gson();
    
    /**
     * Paired Position - Given array of numbers returns the paired positions
     * @param arr - The array of Integer comma separated ex. [1, 3, 5, 8, 3, 5, 3]
     * @return int[][] -[[1,4],[1,6],[2,5],[4,1],[4,6],[5,2],[6,1],[6,4]]
     */
    @RequestMapping(value = "/position/{arr}", method = RequestMethod.GET)
    public ResponseEntity<String> pairedPosition(@PathVariable int[] arr) {
        return ResponseEntity.ok(gson.toJson(logic.pairedPosition(arr)));
    }

}
