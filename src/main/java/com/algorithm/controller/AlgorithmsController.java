package com.algorithm.controller;

import com.algorithm.logic.AlgorithmsLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/algorithms")
public class AlgorithmsController {

    @Autowired
    private AlgorithmsLogic logic;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public void algorithms() {
        System.out.println("Algh!");
    }

}
